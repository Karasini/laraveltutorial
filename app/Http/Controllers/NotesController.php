<?php

namespace App\Http\Controllers;

use App\Note;
use App\Card;
use App\Http\Requests;
use Illuminate\Http\Request;


class NotesController extends Controller
{
    public function store(Request $request, Card $card)
    {
//        $note = new Note;
//
//        $note->body = $request->body;
//
//        $card->notes()->save($note);

//        $card->notes()->save(
//            new Note(['body' => $request->body])
//        );

//        $card->notes()->create([
//            'body' => $request->body
//        ]);
        
        $this->validate($request, [
            'body' => 'required'
        ]);

        $note = new Note($request->all());

        $card->addNote($note, 1);

        return back();
    }

    public function edit(Note $note)
    {
        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note)
    {
        $note->update($request->all());
        
        return back();
    }

}
